<?php
    function ubah_huruf($string){
        $temp_string = "";
        for($i = 0; $i < strlen($string); $i ++) {
            if($string[$i] == 'z') {
                $temp_string = $temp_string . 'a';
            } else {
                $temp_string = $temp_string . chr(ord($string[$i]) + 1);
            }
        }
        return $temp_string;
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo "<br>";
    echo ubah_huruf('developer'); // efwfmpqfs
    echo "<br>";
    echo ubah_huruf('laravel'); // mbsbwfm
    echo "<br>";
    echo ubah_huruf('keren'); // lfsfo
    echo "<br>";
    echo ubah_huruf('semangat'); // tfnbohbu

?>