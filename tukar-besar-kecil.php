<?php
    function tukar_besar_kecil($string){
        $temp_string = "";
        for($i = 0; $i < strlen($string); $i ++) {
            $char = $string[$i];
            $upper = strtoupper($char);
            $lower = strtolower($char);
            if($char == $upper) {
                $temp_string = $temp_string . $lower;
            } else {
                $temp_string = $temp_string . $upper;
            }
        }
        return $temp_string;
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo "<br>";
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo "<br>";
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo "<br>";
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo "<br>";
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
    echo "<br>";

?>